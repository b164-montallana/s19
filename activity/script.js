console.log('Activity 19');

// 3.
let num = 5
const getCube = num ** 3;

// 4. 
console.log(`The cube of ${num} is ${getCube}`);

// 5.
const address = [
    9,
    99,
    13,
    'Burgos'
]

// 6.
const [phase, block, lot, street] = address;
console.log(`phase ${phase}`);
console.log(`blk ${block}`);
console.log(`lot ${lot}`);
console.log(`${street} st.`);

// 7.
const animal = {
    type: 'Horse',
    color: 'Brown',
    size: 'Large'
}

// 8.
const {type, color, size} = animal;
console.log(type);
console.log(color);
console.log(size);

// 9. 
const numbers = [10, 20, 30, 40, 50]

// 10.
numbers.forEach(number => {
    console.log(`${number} is a numbers`);
})

// 11.
const reducedNumber = 0;
const sumNumber = numbers.reduce((x,y) => x +y, reducedNumber);
console.log(sumNumber);

// 12.
class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

// 13.
const dog1 = new Dog(
    'Jackie',
    20,
    'Asky'
)
console.log(dog1);