console.log('Hello WOrld');

// ES6 Updates

// 1. Exponent Operator
// Before
const fisrtNum = Math.pow(8, 2);


// after 
const secondNum = 8 ** 2;


// 2. Template Literals
// Allows to write strings without using the concatenation operator (+)
// uses backticks(``) ${expression}

const interesRate = .1;
const principal = 1000;

console.log(`${principal * interesRate}`);


// 3. Array Destructuring
// Allows us to unpack element in arrays into distinc variables
/*
    Syntax:
        let const [variableNameA, variableNameB, variableNameC] = array;
*/

const fullName = [
    'Juan',
    'Dela',
    'Cruz'
]

// Pre-Array Destructuring
console.warn('Array')
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`${fullName[0]} ${fullName[1]} ${fullName[2]}`);

// Array Destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`${firstName} ${middleName} ${lastName}`);


// 4. Object Destructuring
console.warn('Object')
const person = {
    givenName: 'Jane',
    maidenName: 'Dela',
    familyName: 'Cruz'
}

// Pre-Object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`${person.givenName} ${person.maidenName} ${person.familyName}`);

//Object destructuring
const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`${person.givenName} ${person.maidenName} ${person.familyName}`);

function getFullName({givenName, maidenName, familyName}) {
    console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you.`);
}

getFullName(person);


// 5. Arrow Function
console.warn('Arrow Function');
// traditional function
function printFullName(firstName, middleName, lastName) {
    console.log(`${firstName} ${middleName} ${lastName}`);
}
printFullName('Jhon', 'Dela', 'Cruz')


// Arrow function

let printFullName1 = (firstName,middleInitial,lastName) => {
    console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName1(
    'Jane',
    'D.',
    'Smith'
);


const students = [
    'Jhon',
    'Jane',
    'Jasmine'
];

// Pre-arrow function
students.forEach(function(student) {
    console.log(`${student} is a student`);
})

// Arrow function
students.forEach((student) => {
    console.log(`${student} is a student`);
})

// Implicit return Statement
// pre-Arrow function

// const add = (x, y) => {
//     return x+y
// }

// let total = add(1, 2);
// console.log(total);

// Arrow Function
const add = (x, y) => x + y;

let total = add (1, 2);
console.log(total);





// Default Function Argument Value
console.warn('Default Argument');

const greet = (name = 'User') => {
    return `Good Morning, ${name}!`
}

console.log(greet('Jhon'));
console.log(greet());



// Class-Based Object Blueprints
console.warn('Class-Based Object Bluprint');

class Car {
    constructor(brand, name, year) {
        this.brand = brand;
        this.name = name;
        this.year = year;
    }
}

const myCar = new Car();

console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptop";
myCar.year = "2021";

console.log(myCar);

const myNewCar = new Car (
    'Toyota',
    'Vios',
    2021
)

console.log(myNewCar);




// Ternary Operator
console.warn('TERNARY OPERATOR');

// ? :